/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements.Steamworks;

import DLibX.DConsole;
import java.awt.Color;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import testgame.Elements.StaticElement;
import testgame.util.Hitboxes.HitBox;
import testgame.util.Hitboxes.HitBoxCircle;
import testgame.util.Hitboxes.HitBoxRectangle;
import testgame.util.Hitboxes.HitBoxSegment;
import testgame.util.Vect;

/**
 *
 * @author Kaleb
 */
public class AirShip extends StaticElement {
    private HitBoxSegment hitBox;
    private ArrayList<Line2D.Double> lines = new ArrayList<Line2D.Double>();
    private boolean isRed;
    
    public AirShip(Vect position, boolean isRed) {
        super(position);
        this.isRed = isRed;
        
        if(this.isRed){
            createHexagon(position,100);
        }else{
            createHexagon(position,100);
        }
        this.hitBox = new HitBoxSegment(this.lines,true);
    }

    @Override
    public HitBoxSegment getHitBox() {
        return this.hitBox;
    }

    @Override
    public boolean checkIfInside(HitBoxRectangle r) {
        return this.hitBox.checkIfInside(r);
    }

    @Override
    public boolean checkIfInside(HitBoxCircle c) {
        return this.hitBox.checkIfInside(c);
    }

    @Override
    public boolean checkIfInside(HitBoxSegment s) {
        return this.hitBox.checkIfInside(s);
    }

    @Override
    public void draw(DConsole d) {
       if(this.isRed){
           d.setPaint(Color.RED);
       }else{
           d.setPaint(Color.BLUE);
       }
       this.hitBox.debugDraw(d);
    }
    
    private void createHexagon(Vect pos,double length){
        for(int i = 0; i < 6; i++){ // 6 points 
            lines.add(new Line2D.Double(pos.getX() + (Math.cos(i * Math.PI/3.0 + Math.PI/2.0) * length), pos.getY() + (Math.sin(i * Math.PI/3.0 + Math.PI/2.0) * length) ,
                    pos.getX() + (Math.cos((i+1) * Math.PI/3.0 + Math.PI/2.0) * length), pos.getY() + (Math.sin((i+1) * Math.PI/3.0 + Math.PI/2.0) * length)));
        }
    }
    
}
