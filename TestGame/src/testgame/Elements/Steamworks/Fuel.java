/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements.Steamworks;

import DLibX.DConsole;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import testgame.Elements.PhysicsElement;
import testgame.util.Hitboxes.HitBox;
import testgame.util.Vect;

/**
 *
 * @author Kaleb
 */
public class Fuel extends PhysicsElement {
    private Ellipse2D circle;
    
    public Fuel(Vect position,Ellipse2D ball,double mass, HitBox hBox) {
        super(position, mass,10, hBox);
        this.circle = ball;
    }

    
    @Override
    public void draw(DConsole d) {
        this.circle.setFrame(this.Position.getX(), this.Position.getY(), this.circle.getWidth(), this.circle.getHeight());
        d.setPaint(Color.GREEN);
        d.fill(this.circle);
    }
    
}
