/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements.UI;

import DLibX.DConsole;
import java.awt.Rectangle;
import java.util.ArrayList;

/**
 *
 * @author Kaleb
 */
public class IncrementalDisplay extends Drawable {

    private ArrayList<String> stateTexts;
    private int currentState = 0;
    private Rectangle box; 
    
    
    public IncrementalDisplay(Rectangle box) {
        super(box.getX(), box.getY());
        this.box = box;
        this.stateTexts = new ArrayList<>();
    }
    
    
    public void add(String text){
        this.stateTexts.add(text);
    }
    
    public String getCurrentString(){
        return this.stateTexts.get(this.currentState);
    }
    
    public void update(int stateDiff){
        this.currentState += stateDiff;
        if(this.currentState < 0){
            this.currentState = 0;
        }
        
        if(currentState > this.stateTexts.size() -1){
            this.currentState = this.stateTexts.size()-1;
        }
    }
    
    
    public void reset(){
        this.currentState = 0;
    }
    
    public int getCurrentState(){
        return this.currentState;
    }
    
    

    @Override
    public void draw(DConsole d) {
        d.drawRect(this.box.x, this.box.y, this.box.width, this.box.height);
        d.setOrigin(4);
        d.drawString(this.stateTexts.get(this.currentState), this.box.getX() + this.box.getWidth()/2, this.box.getY() + this.box.getHeight()/3);
        d.setOrigin(0);
    }
    
}
