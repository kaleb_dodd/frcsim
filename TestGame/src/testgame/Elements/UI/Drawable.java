/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements.UI;

import DLibX.DConsole;
import testgame.util.Vect;

/**
 *
 * class that any Element should extend
 */
public abstract class Drawable {
    protected Vect Position;
    
    public Drawable(Vect position){
        this.Position = position;
    }
    
    public Drawable(double x, double y){
        this(new Vect(x,y));
    }
    
    public abstract void draw(DConsole d);
    
    public double getX(){
        return this.Position.getX();
    }
    
    public double getY(){
        return this.Position.getY();
    }
    
    public Vect getPosition(){
        return this.Position;
    }
    
    public void setX(double x){
        this.Position = new Vect(x,this.Position.getY());
    }
    
    public void setY(double y){
        this.Position = new Vect(this.Position.getX(),y);
    }
    
    public void setPosition(Vect pos){
        this.Position = pos;
    }
    
    
}
