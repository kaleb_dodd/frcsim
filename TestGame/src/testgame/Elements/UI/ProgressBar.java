/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements.UI;

import DLibX.DConsole;
import java.awt.Color;

public class ProgressBar extends Drawable {
    private double maxVal;
    private double minVal;
    private double currentFillPercent;
    
    private int width;
    private int height;
    private Color c;
    private boolean drawText;
    private double currentVal;
    private int type =0;
    
    public ProgressBar(int x, int y,int width,int height,double minVal,double maxVal,
            boolean drawText ,Color c,int type) {
        super(x, y);
        this.width = width;
        this.height = height;
        this.minVal = minVal;
        this.maxVal = maxVal;
        this.drawText = drawText;
        this.c = c;
        this.type = type;
    }
    
    public ProgressBar(int x,int y, int width,int height,boolean drawText,Color c){
        this(x,y,width,height,0,100,drawText,c,0);
    }

    @Override
    public void draw(DConsole d) {
        
        if(this.type == 0){ // horizontal and goes out from centre
            d.setPaint(this.c);
            d.setOrigin(4);
            d.fillRect(this.getX(), this.getY(),(this.width*this.currentFillPercent), this.height);
            d.setPaint(Color.BLACK);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }else if(this.type == 1){ // horizontal left to right
            d.setPaint(this.c);
            d.setOrigin(3);
            d.fillRect(this.getX()-(this.width/2), this.getY(), (this.width*this.currentFillPercent), this.height);
            d.setPaint(Color.BLACK);
            d.setOrigin(4);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }else if(this.type == 2){ // vertical from centre
             d.setPaint(this.c);
            d.setOrigin(4);
            d.fillRect(this.getX(), this.getY(),(this.width), (this.height*this.currentFillPercent));
            d.setPaint(Color.BLACK);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }else if(this.type == 3){//verical bottom to top
            d.setPaint(this.c);
            d.setOrigin(6);
            d.fillRect(this.getX()-(this.width/2), this.getY()+(this.height/2), (this.width), (this.height*this.currentFillPercent));
            d.setPaint(Color.BLACK);
            d.setOrigin(4);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }
        if(this.drawText){
             d.drawString(""+(int)this.currentVal+"/"+(int)this.maxVal, this.getX()+(this.width/2), this.getY()+(this.height/2));
        }
    }
    
    public void update(double currentVal){
        this.currentVal = currentVal;
        this.currentFillPercent = (this.currentVal+(Math.abs(minVal))) / (this.maxVal + Math.abs(this.minVal));
        if(this.currentFillPercent > 1){
            this.currentFillPercent = 1;
        }else if(this.currentFillPercent < 0){
            this.currentFillPercent = 0;
        }
    }
    
    public void setMin(double min){
        this.minVal = min;
    }
    
    public void setMax(double max){
        this.maxVal = max;
    }
    
    public void setColor(Color c){
        this.c = c;
    }
    
}