/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements.UI;

import DLibX.DConsole;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Graphics;
import static javafx.scene.text.Font.font;

import testgame.util.Vect;

/**
 *
 * @author Kaleb
 */
public class Button extends Drawable {
    private Rectangle box;
    private String text;
    private Paint boxColour;
    private Paint textColour;
    private Font textFont;
    
    
    public Button(Rectangle box, String text, Color boxColour,Color textColour, 
            Font textFont) {
        super(box.getX(),box.getY());
        this.box = box;
        this.text = text;
        this.boxColour = boxColour;
        this.textColour = textColour;
        this.textFont = textFont;
    }
    
    
    public boolean  isClicked(double x, double y) {
        return (box.contains(x, y));
    }
    

    @Override
    public void draw(DConsole d) {
        d.setPaint(this.boxColour);
        d.draw(this.box);
        
        d.setFont(this.textFont);
        d.setPaint(this.textColour);
        d.setOrigin(4);
        d.drawString(this.text, this.box.getX() + this.box.getWidth()/2 , this.box.getY() + this.box.getHeight()/3);
        d.setOrigin(0);
    }
    
}
