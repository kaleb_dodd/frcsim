/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements.UI;

import DLibX.DConsole;

import java.awt.Color;
import java.awt.Font;

public class ProgressBarColourChanging extends Drawable {
    private double maxVal;
    private double minVal;
    private double currentFillPercent;
    
    private int width;
    private int height;
    private Color full;
    private Color empty;
    private boolean drawText;
    private double currentVal;
    private int type =0;
    private Color c;
    private final Font textFont = new Font("TimesNewRoman", 0, 24);
    private boolean isFollowing = false;
    
    public ProgressBarColourChanging(int x, int y,int width,int height,double minVal,double maxVal,
            boolean drawText ,Color full, Color empty,int type, boolean isFollowing) {
        super(x, y);
        this.width = width;
        this.height = height;
        this.minVal = minVal;
        this.maxVal = maxVal;
        this.drawText = drawText;
        this.full = full;
        this.empty = empty;
        this.type = type;
        this.isFollowing = isFollowing;
    }
    
    public ProgressBarColourChanging(int x,int y, int width,int height,boolean drawText){
        this(x,y,width,height,0,100,drawText,Color.GREEN,Color.RED,0,false);
    }
    
    private void updateColour(){
        int r = (int)Math.abs(this.empty.getRed()-(this.empty.getRed()-this.full.getRed())*this.currentFillPercent);
        int g = (int) Math.abs(this.empty.getGreen()-(this.empty.getGreen()-this.full.getGreen())*this.currentFillPercent);
        int b = (int) Math.abs(this.empty.getBlue()-(this.empty.getBlue()-this.full.getBlue())*this.currentFillPercent);
        
        if(r>255){
            r=255;
        }else if(r<0){
            r=0;
        }
        if(g>255){
            g=255;
        }else if(g<0){
            g=0;
        }
        if(b>255){
            b=255;
        }else if(b<0){
            b=0;
        }
        Color a = new Color(r, g, b);
        //System.out.println("r:"+r);
        //System.out.println("g:"+g);
        //System.out.println("b:"+b);
        this.c = a;
    }

    public void draw(DConsole d, Drawable e) {
        updateColour();
        if(this.isFollowing){
            setPosition(e);
        }
        if(this.type == 0){ // horizontal and goes out from centre
            d.setPaint(this.c);
            d.setOrigin(4);
            d.fillRect(this.getX(), this.getY(),(this.width*this.currentFillPercent), this.height);
            d.setPaint(Color.BLACK);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }else if(this.type == 1){ // horizontal left to right
            d.setPaint(this.c);
            d.setOrigin(3);
            d.fillRect(this.getX()-(this.width/2), this.getY(), (this.width*this.currentFillPercent), this.height);
            d.setPaint(Color.BLACK);
            d.setOrigin(4);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }else if(this.type == 2){ // vertical from centre
             d.setPaint(this.c);
            d.setOrigin(4);
            d.fillRect(this.getX(), this.getY(),(this.width), (this.height*this.currentFillPercent));
            d.setPaint(Color.BLACK);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }else if(this.type == 3){//verical bottom to top
            d.setPaint(this.c);
            d.setOrigin(6);
            d.fillRect(this.getX()-(this.width/2), this.getY()+(this.height/2), (this.width), (this.height*this.currentFillPercent));
            d.setPaint(Color.BLACK);
            d.setOrigin(4);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }
        if(this.drawText){
             d.drawString(""+(int)this.currentVal+"/"+(int)this.maxVal, this.getX()-(this.width/2), this.getY()-(this.height*2.5));
        }
        d.setOrigin(0);
    }
    
    public void update(double currentVal){
        this.currentVal = currentVal;
        this.currentFillPercent = (this.currentVal+(Math.abs(minVal))) / (this.maxVal + Math.abs(this.minVal));
        if(this.currentFillPercent > 1){
            this.currentFillPercent = 1;
        }else if(this.currentFillPercent < 0){
            this.currentFillPercent = 0;
        }
    }
    
    private void setPosition(Drawable d){
        this.setX(d.getX());
        this.setY(d.getY()-15);
    }
    
    public void setMin(double min){
        this.minVal = min;
    }
    
    public void setMax(double max){
        this.maxVal = max;
    }
    
    public void setColor(Color c){
        this.c = c;
    }

    @Override
    public void draw(DConsole d) {
        updateColour();
        if(this.type == 0){ // horizontal and goes out from centre
            d.setPaint(this.c);
            d.setOrigin(4);
            d.fillRect(this.getX(), this.getY(),(this.width*this.currentFillPercent), this.height);
            d.setPaint(Color.BLACK);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }else if(this.type == 1){ // horizontal left to right
            d.setPaint(this.c);
            d.setOrigin(3);
            d.fillRect(this.getX()-(this.width/2), this.getY(), (this.width*this.currentFillPercent), this.height);
            d.setPaint(Color.BLACK);
            d.setOrigin(4);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }else if(this.type == 2){ // vertical from centre
             d.setPaint(this.c);
            d.setOrigin(4);
            d.fillRect(this.getX(), this.getY(),(this.width), (this.height*this.currentFillPercent));
            d.setPaint(Color.BLACK);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }else if(this.type == 3){//verical bottom to top
            d.setPaint(this.c);
            d.setOrigin(6);
            d.fillRect(this.getX()-(this.width/2), this.getY()+(this.height/2), (this.width), (this.height*this.currentFillPercent));
            d.setPaint(Color.BLACK);
            d.setOrigin(4);
            d.drawRect(this.getX(), this.getY(), this.width, this.height);
            d.setOrigin(4);
           
        }
        if(this.drawText){
            d.setFont(textFont);
             d.drawString(""+(int)this.currentVal, this.getX(), this.getY());
        }
    }
    
}
