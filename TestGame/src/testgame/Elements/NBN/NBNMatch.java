/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements.NBN;

import DLibX.DConsole;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import testgame.Elements.GenericMatch;
import testgame.Elements.UI.ProgressBarColourChanging;
import testgame.Elements.UI.TextBox;

/**
 *
 * @author Kaleb
 */
public class NBNMatch extends GenericMatch {
    private Rectangle redScore;
    private Rectangle blueScore;
    
    private TextBox redScoreDisplay;
    private TextBox blueScoreDisplay;
    
    private ProgressBarColourChanging timer; 
    
    
    private final Color redBoxColor = new Color(220,0,0,200);
    private final Color blueBoxColor = new Color(0,0,255,200);
    private final Color textColor = Color.BLACK;
    private final Font textFont = new Font("TimesNewRoman", 0, 24);
   
    
    public NBNMatch() {
        super(105, 15, 30, 5, 25);
        
        this.redScore = new Rectangle(440, 10, 50, 30);
        this.blueScore = new Rectangle(560,10,50,30);
        this.timer = new ProgressBarColourChanging(525, 80, 300, 40, 0, 120, true,Color.GREEN , Color.RED, 0, false);
        
        this.redScoreDisplay = new TextBox(this.redScore, "0",true, redBoxColor, textColor, textFont);
        this.blueScoreDisplay = new TextBox(this.blueScore, "0",true, blueBoxColor, textColor, textFont);
        
    }

    @Override
    public void draw(DConsole d) {
        this.redScoreDisplay.setText("" + this.getRedScore());
        this.blueScoreDisplay.setText("" + this.getBlueScore());
        
        this.timer.update(120 - this.getCurrentTime());
        
        this.timer.draw(d);
        this.blueScoreDisplay.draw(d);
        this.redScoreDisplay.draw(d);
        
        
    }
    
}
