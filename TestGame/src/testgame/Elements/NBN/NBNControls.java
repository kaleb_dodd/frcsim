/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements.NBN;

/**
 *
 * @author Kaleb
 */
public class NBNControls {
    private double movementX,movementY, rotation;
    private boolean intake,shooting;
    
    public NBNControls(double movementX, double movementY, double rotation, boolean intake, boolean shooting){
        this.movementX = movementX;
        this.movementY = movementY;
        this.rotation = rotation;
        this.intake = intake;
        this.shooting = shooting;
    }
    
    public void update(double movementX, double movementY, double rotation, boolean intake, boolean shooting){
        this.movementX = movementX;
        this.movementY = movementY;
        this.rotation = rotation;
        this.intake = intake;
        this.shooting = shooting;
    }
    
    public double getMovementX(){
        return this.movementX;
    }
    
    public double getMovementY(){
        return this.movementY;
    }
    
    public double getRotation(){
        return this.rotation;
    }
    
    public boolean getIntake(){
        return this.intake;
    }
    
    public boolean getShooting(){
        return this.shooting;
    }
    
    
    
    
    
    
}
