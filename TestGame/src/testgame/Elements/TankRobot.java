/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements;

import java.awt.geom.Rectangle2D;
import testgame.util.Hitboxes.HitBox;
import testgame.util.Vect;

/**
 *
 * @author Kaleb
 */
public abstract class TankRobot extends Robot {
    
    public TankRobot(Vect position, Rectangle2D robot, double mass,double drag ,HitBox hBox) {
        super(position, robot, mass,drag ,hBox);
    }
    
    
    
}
