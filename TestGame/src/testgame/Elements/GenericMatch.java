/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements;

import DLibX.DConsole;

/**
 *
 * @author Kaleb
 */
public abstract class GenericMatch {
    private long teleopLength;
    private long autoLength;
    private long endGameStart; 
    private long startTime;
    private long currentTime; 
    private int foulValue;
    private int techFoulValue;
    private matchState currentMatchState; 
    private int redScore;
    private int blueScore;
    private int redScoreAcutal;
    private int blueScoreActual; 
    private int redFoulCount;
    private int blueFoulCount;
    private int redTechFoulCount;
    private int blueTechFoulCount;
    private String red1,red2,blue1,blue2;
    
    
    public enum matchState {
        AUTO,
        TELEOP,
        ENDGAME,
        DONE, 
    }
    
 
    public GenericMatch(long teleopLength, long autoLength, long endGameStart, int foulValue, int techFoulValue) {
        this.teleopLength = teleopLength * 1000;
        this.autoLength = autoLength * 1000;
        this.endGameStart = endGameStart * 1000;
        this.foulValue = foulValue;
        this.techFoulValue = techFoulValue;  
    }
    
    public abstract void draw(DConsole d);
    
    
    
    public void setRedAlliance(String red1, String red2){
        this.red1 = red1;
        this.red2 = red2;
    }
    
    public void setBlueAlliance(String blue1, String blue2){
        this.blue1 = blue1;
        this.blue2 = blue2;
    }
    
  
    
    
    public void startMatch(long currentTime){
        this.redScore = 0;
        this.blueScore = 0;
        this.redFoulCount = 0;
        this.redTechFoulCount = 0;
        this.blueFoulCount = 0;
        this.blueTechFoulCount = 0;
        this.startTime = currentTime; 
    }
    
    
    public void update(long currentTime) {
        this.currentTime = currentTime - this.startTime;
        
        if(this.currentTime <= this.autoLength){
            this.currentMatchState = matchState.AUTO;
        } else if(this.currentTime <= this.autoLength + this.teleopLength - this.endGameStart){
            this.currentMatchState = matchState.TELEOP;
        } else if(this.currentTime <= this.autoLength + this.teleopLength){
            this.currentMatchState = matchState.ENDGAME;
        } else {
            this.currentMatchState = matchState.DONE;
        } 
        
        this.redScoreAcutal = this.redScore + (this.redFoulCount * this.foulValue) 
                + (this.redTechFoulCount * this.techFoulValue);
        this.blueScoreActual = this.blueScore + (this.blueFoulCount * this.foulValue) 
                + (this.blueTechFoulCount * this.techFoulValue);
    }
    
    public void addScore(int red, int blue){
        this.redScore += red;
        this.blueScore += blue;
    }
    
    public void addFoul(boolean red){
        if(red){
            this.redFoulCount++;
        } else {
            this.blueFoulCount++;
        }
    }
    
    public void addTechFoul(boolean red){
        if(red){
            this.redTechFoulCount++;
        } else {
            this.blueTechFoulCount++;
        }
    }
    
    public long getCurrentTime(){
        return this.currentTime / 1000;
    }
    
    public matchState getCurrentMatchState(){
        return this.currentMatchState;
    }
    
    public int getRedFoulCount(){
        return this.redFoulCount;
    }
    
    public int getBlueFoulCount(){
        return this.blueFoulCount;
    }
     
    public int getRedTechFoulCount(){
        return this.redTechFoulCount;
    }
      
    public int getBlueTechFoulCount(){
        return this.blueTechFoulCount;
    }
    
    public int getRedScore(){
        return this.redScoreAcutal;
    }
    
    public int getBlueScore(){
        return this.blueScoreActual;
    }
    
    
    
}
