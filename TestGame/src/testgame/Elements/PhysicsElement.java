/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements;

import testgame.Elements.UI.Drawable;
import DLibX.DConsole;
import testgame.util.Hitboxes.HitBox;
import testgame.util.Hitboxes.HitBoxCircle;
import testgame.util.Hitboxes.HitBoxRectangle;
import testgame.util.Hitboxes.HitBoxSegment;
import testgame.util.Vect;

/**
 *
 * @author balke
 */
public abstract class PhysicsElement extends Drawable {
    protected Vect Velocity;
    protected Vect Acceleration;
    protected Vect Momentum;
    protected Vect Force;
    protected double Mass;
    protected HitBox hitbox;
    protected double dragConstant;
    protected boolean rotatable = false;
    protected double heading = 0;
    protected double headingVel = 0;
    protected double headingAccel = 0;
    protected double headingMomentum = 0;
    protected double headingForce = 0;
    protected double headingDragConstant;
    
    
    
    public PhysicsElement(Vect position,double mass,double drag,boolean rotatable,double rotationalDrag,HitBox hBox) {
        super(position);
        this.Mass = mass;
        this.rotatable =  rotatable;
        this.hitbox = hBox;
        this.dragConstant = drag;
        this.headingDragConstant = rotationalDrag;
        this.Velocity = new Vect(0, 0);
        
    }
    
    public PhysicsElement(Vect position,double mass,double drag,HitBox hBox){
        this(position, mass, drag,false,0, hBox);
    }
    
    public void rotate(double force, double deltaTime) {
        deltaTime = deltaTime / 1000.0;
        
        this.headingForce = force;
        double wind = rotationalAirResistance(this.headingVel);
        this.headingForce -= wind;
        this.headingAccel = this.headingForce / this.Mass;
        this.headingVel += this.headingAccel * deltaTime;
        
        if(Math.abs(this.headingVel) < 10 ){
            this.headingVel = 0;
        }
        
        this.heading += this.headingVel * deltaTime;
        this.headingMomentum = this.headingVel * this.Mass;
        this.headingForce = this.headingAccel * this.Mass;
        
        if(this.hitbox.getIsFollowing()){
            this.hitbox.update(this,deltaTime); 
        }
    }
    
    public void updatePosition(Vect Force,double deltaTime){
        deltaTime = deltaTime / 1000.0;
        
        this.Force = Force;
        
        Vect wind = airResistance(this.Velocity);
        
        this.Force = this.Force.add(wind);
        
        
        
        this.Acceleration = new Vect(this.Force.getX()/this.Mass, this.Force.getY()/this.Mass);
        this.Velocity = this.Velocity.add(this.Acceleration.scalarMult(deltaTime));
        
        System.out.println("FORCE" + this.Force.mag());
        System.out.println("VEL" + this.Velocity.mag());
        
        if(this.Force.mag() < 40 && this.Velocity.mag() < 6){
            this.Velocity = new Vect(0,0);
        }
        
        this.Position = this.Position.add(this.Velocity.scalarMult(deltaTime));
        
        this.Momentum = this.Velocity.scalarMult(this.Mass);
        this.Force = this.Acceleration.scalarMult(this.Mass);
        
        if(this.hitbox.getIsFollowing()){
            this.hitbox.update(this,deltaTime); 
        }
        
        
    }
    
    public boolean checkIfInside(HitBoxCircle c){
        return this.hitbox.checkIfInside(c);
    }
    
    public boolean checkIfInside(HitBoxRectangle c){
        return this.hitbox.checkIfInside(c);
    }
    
    public boolean checkIfInside(HitBoxSegment c){
        return this.hitbox.checkIfInside(c);
    }
    
    public void drawHitbox(DConsole d){
        this.hitbox.debugDraw(d);
    }
    
    public HitBox getHitBox(){
        return this.hitbox;
    }
    
    public void reset(){
        this.setVelocity(new Vect(0,0));
        this.setAcceleration(new Vect(0,0));
    }
    
    
    public Vect getVelocity(){
        return this.Velocity;
    }
    
    public Vect getAcceleration(){
        return this.Acceleration;
    }
    
    public Vect getForce(){
        return this.Force;
    }
    
    public Vect getMomentum(){
        return this.Momentum;
    }
    
    public void setVelocity(Vect Vel){
        this.Velocity = Vel;
    }
    
    public void setAcceleration(Vect Accel){
        this.Acceleration = Accel;
    }
    
    public Vect airResistance(Vect Velocity){
        return Velocity.scalarMult(this.dragConstant).rotate(180);
    }
    
    public double rotationalAirResistance(double velocity){
        return velocity * this.headingDragConstant;
    }
    
}
