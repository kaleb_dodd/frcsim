/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements;

import DLibX.DConsole;
import java.awt.geom.Rectangle2D;
import testgame.util.Hitboxes.HitBox;
import testgame.util.Vect;

/**
 *
 * @author balke
 */
public abstract class Robot extends PhysicsElement{
    private Rectangle2D sizeRect;
    
    public Robot(Vect position,Rectangle2D robot ,double mass,double drag ,HitBox hBox) {
        super(position, mass,drag ,hBox);
        this.sizeRect = robot;
    }

    @Override
    public void draw(DConsole d) {
        
        this.sizeRect.setFrame(this.Position.getX(),this.Position.getY(),100,100);
        System.out.println("RECT W " + this.sizeRect.getWidth());
        System.out.println("RECT H " + this.sizeRect.getHeight());
        d.fill(this.sizeRect);
    }
    
    
    
}
