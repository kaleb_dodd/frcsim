/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements;

import DLibX.DConsole;
import java.awt.Color;
import testgame.util.Hitboxes.HitBox;
import testgame.util.Hitboxes.HitBoxCircle;
import testgame.util.Hitboxes.HitBoxRectangle;
import testgame.util.Hitboxes.HitBoxSegment;
import testgame.util.Vect;

/**
 *
 * @author balke
 */
public class Wall extends StaticElement {
    private HitBoxRectangle hitBox;
   
    
    public Wall(Vect position,HitBoxRectangle hBox) {
        super(position);
        this.hitBox = hBox;
    }
    
    @Override
    public HitBoxRectangle getHitBox(){
        return this.hitBox;
    }

    @Override
    public boolean checkIfInside(HitBoxRectangle r) {
        return this.hitBox.checkIfInside(r);
    }

    @Override
    public boolean checkIfInside(HitBoxCircle c) {
        return this.hitBox.checkIfInside(c);
    }

    @Override
    public boolean checkIfInside(HitBoxSegment s) {
       return this.hitBox.checkIfInside(s);
    }

    @Override
    public void draw(DConsole d) {
        d.setPaint(Color.DARK_GRAY);
        d.fill(this.hitBox.getRect());
    }
    
}
