/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.Elements;

import testgame.Elements.UI.Drawable;
import testgame.util.Hitboxes.HitBox;
import testgame.util.Hitboxes.HitBoxCircle;
import testgame.util.Hitboxes.HitBoxRectangle;
import testgame.util.Hitboxes.HitBoxSegment;
import testgame.util.Vect;

/**
 *
 * @author balke
 */
public abstract class StaticElement extends Drawable{
    
    
    public StaticElement(Vect position){
        super(position);
        
    }
    
    public abstract HitBox getHitBox();
    
    public abstract boolean checkIfInside(HitBoxRectangle r);
    
    public abstract boolean checkIfInside(HitBoxCircle c);
    
    public abstract boolean checkIfInside(HitBoxSegment s);
    
    
}
