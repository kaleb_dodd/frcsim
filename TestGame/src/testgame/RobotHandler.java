/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame;

import DLibX.DConsole;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import testgame.Elements.NBN.NBNControls;
import testgame.Elements.Robot;
import testgame.Elements.testbot;
import testgame.FRCSimulator.GameType;
import testgame.util.GamePads;
import testgame.util.Hitboxes.HitBoxRectangle;
import testgame.util.Vect;

/**
 *
 * @author Kaleb
 */
public class RobotHandler {
    
    private GameType currentGame;
    private ArrayList<String> playerSelections;
    private ArrayList<String> robotSelections;
    private GamePads controllers;
    
    
    private ArrayList<Robot> robots;
    private ArrayList<NBNControls> playersInputs;
    
    public RobotHandler(GameType currentGame, ArrayList<String> playerSelections, ArrayList<String> robotSelections){
        this.currentGame = currentGame;
        this.playerSelections = playerSelections;
        this.robotSelections = robotSelections; 
        this.robots = new ArrayList<>();
        this.controllers = new GamePads();
        this.playersInputs = new ArrayList<>();
        
    }
    
    
    public void initMatch(){
        for(int i = 0; i < this.robotSelections.size(); i++){
            if(this.playerSelections.get(i).equals("NONE")){
                break;
            }
             if(this.currentGame == GameType.NOTHING_BUT_NET){
                 if(this.robotSelections.get(i).equals("1114M")){
                     Vect pos = new Vect(500 + i*50, 0);
                     Rectangle2D rect = new Rectangle(500 + i*50, 0, 10, 10);
                     HitBoxRectangle hBox = new HitBoxRectangle(rect, true);
                     this.robots.add(new testbot(pos, rect, 10, 20, hBox));
                 }
             }
        }
    }
    
    public void update(double dt){
        this.controllers.update();
        
        int controllerCount = 0;
      
        this.playersInputs.clear();
        for(int i = 0; i < this.playerSelections.size(); i++) {
            if(this.playerSelections.get(i).equals("PLAYER")){
                this.playersInputs.add(new NBNControls(this.controllers.getLeftX(controllerCount), this.controllers.getLeftY(controllerCount),
                        this.controllers.getRightX(controllerCount), this.controllers.getButton(controllerCount, "A"), this.controllers.getButton(controllerCount, "B")));
                controllerCount++;
            }
        }
        
        for(int i = 0; i < this.robots.size(); i++){
            Vect force = new Vect(0,0);
            
            if(this.playerSelections.get(i).equals("PLAYER")){
                force = new Vect(this.playersInputs.get(i).getMovementX() * 4000, -this.playersInputs.get(i).getMovementY() * 4000);
            }
            System.out.println("X " + force.getX());
            System.out.println("Y " + force.getY());
            System.out.println("POS " + this.robots.get(i).getPosition().toString());
            
            this.robots.get(i).updatePosition(force, dt);
        }
        
        
    }
    
    public void draw(DConsole d){
        for(Robot r : this.robots){
            r.draw(d);
        }
    }
    
    
    
}
