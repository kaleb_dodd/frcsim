/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame;

import DLibX.DConsole;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Rectangle;
import testgame.Elements.UI.Button;

/**
 *
 * @author Kaleb
 */

public class GameSelectMenu {
    private final Color buttonColor = Color.BLUE;
    private final Color textColor = Color.BLACK;
    private final Font textFont = new Font("TimesNewRoman", 0, 24);
    private final String button1Text = "BACK";
    private final String button2Text = "Nothing But Net (VRC 2016)";
    private final Rectangle button1Box = new Rectangle(300, 300, 100, 30);
    private final Rectangle button2Box = new Rectangle(100, 100, 350, 30);
    
    private Button NothingButNet;
    private Button back; 

    
   public GameSelectMenu(){ 
       this.NothingButNet = new Button(this.button2Box, this.button2Text, (this.buttonColor), this.textColor, textFont);
       this.back = new Button(this.button1Box, this.button1Text, (this.buttonColor), this.textColor, textFont);
   }     

    
    public int update(double mouseX, double mouseY, boolean mouseButton) {
        int state = 0;
        
        if(mouseButton) {
            if(this.back.isClicked(mouseX, mouseY)){
                state = 1;
            } else if(this.NothingButNet.isClicked(mouseX, mouseY)) {
                state = 2;
            }
        }
        
        
        
        return state;
    }
    
    
    public void draw(DConsole d) {
        this.NothingButNet.draw(d);
        this.back.draw(d);
    }
    
    
    
}
