/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.util;

import DLibX.DConsole;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

/**
 *
 * @author balke
 */
public class ResizableImage {
    private BufferedImage source; 
    private Rectangle bounds;
    
    public ResizableImage(BufferedImage src, int x, int y){
        this.source = src;
        this.bounds = new Rectangle(x, y, src.getWidth(), src.getHeight());
    }
    
    public void setPos(int x, int y){
        this.bounds.setRect(x, y, this.bounds.getWidth(), this.bounds.getHeight());
    }
    
    public void setSize(Rectangle boundingRect){
        this.bounds = boundingRect;
        int w = this.source.getWidth();
        int h = this.source.getHeight();
        BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        AffineTransform at = new AffineTransform();
        double scaleW =  (this.bounds.getWidth() / w);
        double scaleH = (this.bounds.getHeight() / h);
        at.scale(scaleW, scaleH);
        AffineTransformOp scaleOp = 
        new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        after = scaleOp.filter(this.source, after);
        this.source = after; 
    }
    
    
    
    public void draw(DConsole d){
        d.setOrigin(0);
        //d.drawRect(this.bounds.getX(), this.bounds.getY(), this.bounds.getWidth(), this.bounds.getHeight());
        d.drawImage(this.source, this.bounds.getX(), this.bounds.getY());
        d.setOrigin(4);
    }
    
    
    
}
