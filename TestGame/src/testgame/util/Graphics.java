/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.util;

import DLibX.DConsole;
import java.awt.Rectangle;

/**
 *
 * @author balke
 */

public class Graphics {
    private double width;
    private double height;
    private static Graphics instance;
    private Rectangle sizingRect;
    
    
    private Graphics(){
        
    }
     
    public static Graphics getInstance(){
        if(instance == null){
            instance = new Graphics();
        }
        return instance;
    }

    public void debugDraw(DConsole d){
        d.drawRect(this.sizingRect.x, this.sizingRect.y, this.sizingRect.getWidth(), this.sizingRect.getHeight());
    }
   
    
    public void setSize(int width, int height){
        this.width = width;
        this.height = height;
    }
    
    public void setSizingRect(int x, int y, int width, int height){
        this.sizingRect = new Rectangle((int)x,(int) y,(int) width,(int) height);
    }
    
    
    public int getScreenXPos(double percent){
        return (int)Math.round(percent * this.width);
    }
    
    public int getScreenYPos(double percent){
        return (int)Math.round(percent * this.height);
    }
    
    public int getFieldXSize(double xSize){
        xSize /= 54;
        xSize *= this.sizingRect.getWidth();
        return (int) Math.round(xSize);
    }
    
    public int getFieldYSize(double ySize){
        ySize /= 27;
        ySize *= this.sizingRect.getHeight();
        return (int) Math.round(ySize);
    }
    
    public int getFieldXPixle(double xPos){
        xPos /= 54;
        xPos *= this.sizingRect.getWidth();
        xPos += (this.sizingRect.getX() - (this.sizingRect.getWidth()/2));
        return (int) Math.round(xPos); 
    }
    
    public int getFieldYPixle(double yPos){
        yPos /= 27;
        yPos *= this.sizingRect.getHeight();
        yPos += (this.sizingRect.getY() - (this.sizingRect.getHeight()/2));
        return (int) Math.round(yPos); 
    }
    
}
