/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.util;

import DLibX.DConsole;

/**
 *
 * @author balke
 */
public class Vect {
    private double x;
    private double y;

    public Vect(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public Vect(double magnitude, double heading, boolean blank){
        this(magnitude * Math.cos(Math.toRadians(heading)),magnitude * Math.sin(Math.toRadians(heading)));
    }
	
    public double getX() {
        return x;	
    }
	
    public double getY() {
        return y;
    }
    
    public double getHeading(){
        return Math.toDegrees(Math.atan2(this.y, this.x));
    }

    public Vect(Vect clone) {
        this.x = clone.x;
        this.y = clone.y;
    }	

    public Vect add(Vect b) {
        return new Vect(this.x + b.x, this.y + b.y);
    }

    public Vect sub(Vect b) {
    	return new Vect(this.x - b.x, this.y - b.y);
    }
    
    public Vect scalarMult(double c){
        return new Vect (this.x * c, this.y *c);
    }
    
    public static Vect fromAngle(double c) {
    	c = Math.toRadians(c);
    	return new Vect(Math.cos(c), Math.sin(c));
    }

    public Vect unit() {
        double magnitude = this.mag();
        return new Vect(this.x / magnitude, this.y / magnitude);
    }

    public double magSq() {
        return this.dot(this);
    }
	
    public double dot(Vect b) {
        return this.x * b.x + this.y * b.y;
    }

    public double mag() {
        return Math.sqrt(this.magSq());
    }
    
    public double scalarProjectOnto(Vect b) {
    	return this.dot(b) / b.mag();
    }
    
    public Vect projectOnto(Vect b) {
        return b.scalarMult(this.dot(b) / b.magSq());
    }
    
    public Vect rotate(double degrees) {
    	double angle = Math.toRadians(degrees);
    	double x = this.x*Math.cos(angle)-this.y*Math.sin(angle);
    	double y = this.x*Math.sin(angle)+this.y*Math.cos(angle);
    	return new Vect(x,y);
    }
    
    public void draw(Vect pos,double mult ,DConsole d){
        d.drawLine(pos.getX(), pos.getY(), (this.x*mult)+pos.getX(), (this.y*mult)+pos.getY());
    }
    
    @Override
    public String toString() {
    	return "(" + x + "," + y + ")";
    }
    
}
