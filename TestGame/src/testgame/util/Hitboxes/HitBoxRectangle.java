/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.util.Hitboxes;

import DLibX.DConsole;
import java.awt.geom.Rectangle2D;
import testgame.Elements.PhysicsElement;

/**
 *
 * @author balke
 */
public class HitBoxRectangle extends HitBox {
    private Rectangle2D rect; 
    
    public HitBoxRectangle(Rectangle2D r,boolean following) {
        super(following);
        this.rect = r;
    }

    public Rectangle2D getRect(){
        return this.rect;
    }
    
    
    @Override
    public void update(PhysicsElement e, double dt) {
        if(this.following){
            this.rect.setRect(this.rect.getX() + e.getVelocity().getX()*dt, this.rect.getY() + e.getVelocity().getY()*dt, 
                    this.rect.getWidth(), this.rect.getHeight());
        }
    }

    @Override
    public boolean checkIfInside(HitBoxRectangle r) {
        return this.rect.intersects(r.getRect());
    }

    @Override
    public boolean checkIfInside(HitBoxCircle c) {
        return this.rect.intersects(c.getCircle().getBounds2D());
    }

    @Override
    public boolean checkIfInside(HitBoxSegment s) {
        for(int i = 0; i < s.getSegments().size(); i++){
            if(s.getSegments().get(i).intersects(this.rect)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void debugDraw(DConsole d) {
        d.drawRect(this.rect.getX(), this.rect.getY(), this.rect.getWidth(), this.rect.getHeight());
    }
    
}
