/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.util.Hitboxes;

import DLibX.DConsole;
import java.awt.Shape;
import testgame.Elements.PhysicsElement;

/**
 *
 * @author balke
 */
public abstract class HitBox {
    protected boolean following;
    
    public HitBox(boolean following){
        this.following = following;
    }
    
    public boolean getIsFollowing(){
        return this.following;
    }
    
    public abstract void update(PhysicsElement e, double dt);
    
    
    
    public abstract boolean checkIfInside(HitBoxRectangle r);
    
    public abstract boolean checkIfInside(HitBoxCircle c);
    
    public abstract boolean checkIfInside(HitBoxSegment s);
    
    public abstract void debugDraw(DConsole d);

    
   

    
    
    
}
