/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.util.Hitboxes;

import DLibX.DConsole;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import testgame.Elements.PhysicsElement;

/**
 *
 * @author balke
 */
public class HitBoxSegment extends HitBox {
    
    private ArrayList<Line2D.Double> lineSegments; 
    
    
    public HitBoxSegment(ArrayList<Line2D.Double> lines, boolean following){
        super(following);
        this.lineSegments = lines;
        
    }
    
    @Override
    public void update(PhysicsElement e, double dt){
        if(this.following){
            for(int i = 0; i < lineSegments.size(); i++){ // go through each line and update its position based on the moving elements speed
                Point2D oldSP = lineSegments.get(i).getP1();
                Point2D oldEP = lineSegments.get(i).getP2();

                oldSP.setLocation(oldSP.getX() + e.getVelocity().getX()*dt, oldSP.getY() + e.getVelocity().getY()*dt);
                oldEP.setLocation(oldEP.getX() + e.getVelocity().getX()*dt, oldEP.getY() + e.getVelocity().getY()*dt);
                lineSegments.get(i).setLine(oldSP,oldEP);
            }
        }
    }
    
    @Override
    public void debugDraw(DConsole d){
        for(int i = 0; i < this.lineSegments.size(); i++){
            Point2D oldSP = lineSegments.get(i).getP1();
            Point2D oldEP = lineSegments.get(i).getP2();
            d.drawLine(oldSP.getX(), oldSP.getY(),oldEP.getX(), oldEP.getY());
        }
    }
    
    public ArrayList<Line2D.Double> getSegments(){
        return this.lineSegments;
    }
    
    @Override
    public boolean checkIfInside(HitBoxSegment s){ // need to make this work when the objects are completely inside each other
        for(int i = 0; i < this.lineSegments.size(); i++){ // for every one of my line segments
            for(int j = 0; j < s.getSegments().size() ; j++){ // check if any of their line segments intersect 
                if(this.lineSegments.get(i).intersectsLine(s.getSegments().get(i))){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean checkIfInside(HitBoxRectangle r) {
        for(int i = 0; i < this.lineSegments.size(); i++){
            if(this.lineSegments.get(i).intersects(r.getRect())){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean checkIfInside(HitBoxCircle c) {
        for(int i = 0; i < this.lineSegments.size(); i++){
            if(this.lineSegments.get(i).intersects(c.getCircle().getBounds2D())){
                return true;
            }
        }
        return false;
    }

  
    
    
}
