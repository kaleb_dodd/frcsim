/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.util.Hitboxes;

import DLibX.DConsole;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import testgame.Elements.PhysicsElement;

/**
 *
 * @author balke
 */
public class HitBoxCircle extends HitBox {
    private Ellipse2D circle;
    
    public HitBoxCircle(Ellipse2D.Double circle,boolean following) {
        super(following);
        this.circle = circle;
    }
    
    public Ellipse2D getCircle(){
        return this.circle;
    }

    @Override
    public void update(PhysicsElement e, double dt) {
        if(this.following){
            this.circle.setFrame(this.circle.getX() + e.getVelocity().getX()*dt, this.circle.getY() + e.getVelocity().getY() * dt,
                    this.circle.getWidth(), this.circle.getHeight());
        }
    }

    @Override
    public boolean checkIfInside(HitBoxRectangle r) {
        return this.circle.getBounds2D().intersects(r.getRect());
    }

    @Override
    public boolean checkIfInside(HitBoxCircle c) {
        return this.circle.getBounds2D().intersects(c.getCircle().getBounds2D());
    }

    @Override
    public boolean checkIfInside(HitBoxSegment s) {
        for(int i = 0; i < s.getSegments().size(); i++){
            if(s.getSegments().get(i).intersects(this.circle.getBounds2D())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void debugDraw(DConsole d) {
        d.drawEllipse(this.circle.getX(), this.circle.getY(), this.circle.getWidth(), this.circle.getHeight());
    }
    
}
