/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame.util;

import com.studiohartman.jamepad.ControllerManager;
import com.studiohartman.jamepad.ControllerState;
import java.util.ArrayList;



/**
 *
 * @author Kaleb
 */


public class GamePads {
    
    private ControllerManager controllers; 
    private ArrayList<ControllerState>  controllerStates;
    
    public GamePads(){
        this.controllers = new ControllerManager();
        this.controllers.initSDLGamepad();
        this.controllerStates = new ArrayList<>();
        
    }
    
   
    
    public void update(){
        this.controllerStates.clear();
        for(int i = 0; i < 4; i++) {
            if(this.controllers.getState(i).isConnected){
                this.controllerStates.add(this.controllers.getState(i));
            }
        }
    }
    
    
    public boolean isControllerConnected(int controller){
        if(controller < 0 || controller > this.controllerStates.size()-1){
            return false;
        }
        
        return this.controllerStates.get(controller).isConnected;
    }
    
    public double getLeftX(int controller){
        if(controller < 0 || controller > this.controllerStates.size()-1){
            return 0;
        }
        
        if(!this.controllerStates.get(controller).isConnected){
            return 0;
        }
        
        double val = this.controllerStates.get(controller).leftStickX;
        if(Math.abs(val) < 0.1){
            val = 0;
        }
        
        return val;
    }
    
    public double getLeftY(int controller){
        if(controller < 0 || controller > this.controllerStates.size()-1){
            return 0;
        }
        
        if(!this.controllerStates.get(controller).isConnected){
            return 0;
        }
        
        double val = this.controllerStates.get(controller).leftStickY;
        
        if(Math.abs(val) < 0.1){
            val = 0;
        }
        
        return val;
        
    }
    
    public double getRightX(int controller){
        if(controller < 0 || controller > this.controllerStates.size()-1){
            return 0;
        }
        
        if(!this.controllerStates.get(controller).isConnected){
            return 0;
        }
        
        
        double val = this.controllerStates.get(controller).rightStickX;
        
        if(Math.abs(val) < 0.1){
            val = 0;
        }
        
        return val;
    }
    
    public double getRightY(int controller){
        if(controller < 0 || controller > this.controllerStates.size()-1){
            return 0;
        }
        
        if(!this.controllerStates.get(controller).isConnected){
            return 0;
        }
        
        double val = this.controllerStates.get(controller).rightStickY;
        
        if(Math.abs(val) < 0.1){
            val = 0;
        }
        
        return val;
    }
    
    public double getLeftTrigger(int controller){
        if(controller < 0 || controller > this.controllerStates.size()-1){
            return 0;
        }
        
        if(!this.controllerStates.get(controller).isConnected){
            return 0;
        }
        
        return this.controllerStates.get(controller).leftTrigger;
    }
    
    public double getRightTrigger(int controller){
        if(controller < 0 || controller > this.controllerStates.size()-1){
            return 0;
        }
        
        if(!this.controllerStates.get(controller).isConnected){
            return 0;
        }
        
        return this.controllerStates.get(controller).rightTrigger;
    }
    
    
    public boolean getButton(int controller, String button){
        
        if(controller < 0 || controller > this.controllerStates.size()-1){
            return false;
        }
        
        if(!this.controllerStates.get(controller).isConnected){
            return false;
        }
        
        if(button.equalsIgnoreCase("A")){
            return this.controllerStates.get(controller).a;
        } else if(button.equalsIgnoreCase("B")){
            return this.controllerStates.get(controller).b;
        } else if(button.equalsIgnoreCase("X")){
            return this.controllerStates.get(controller).x;
        } else if(button.equalsIgnoreCase("Y")){
            return this.controllerStates.get(controller).y;
        } else if(button.equalsIgnoreCase("RB")){
            return this.controllerStates.get(controller).rb;
        } else if(button.equalsIgnoreCase("LB")){
            return this.controllerStates.get(controller).lb;
        } else if(button.equalsIgnoreCase("RS")){
            return this.controllerStates.get(controller).rightStickClick;
        } else if(button.equalsIgnoreCase("LS")){
            return this.controllerStates.get(controller).leftStickClick;
        } else if(button.equalsIgnoreCase("START")){
            return this.controllerStates.get(controller).start;
        } else if(button.equalsIgnoreCase("BACK")){
            return this.controllerStates.get(controller).back;
        } else if(button.equalsIgnoreCase("DUP")){
            return this.controllerStates.get(controller).dpadUp;
        } else if(button.equalsIgnoreCase("DDOWN")){
            return this.controllerStates.get(controller).dpadDown;
        } else if(button.equalsIgnoreCase("DLEFT")){
            return this.controllerStates.get(controller).dpadLeft;
        } else if(button.equalsIgnoreCase("DRIGHT")){
            return this.controllerStates.get(controller).dpadRight;
        } else {
            return false;
        }
        
        
        
        
     
    }
    
    
    
    
}
