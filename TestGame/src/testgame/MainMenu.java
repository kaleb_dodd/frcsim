/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame;

import DLibX.DConsole;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Rectangle;
import testgame.Elements.UI.Button;

/**
 *
 * @author Kaleb
 */

public class MainMenu {
    private final Color buttonColor = Color.BLUE;
    private final Color textColor = Color.BLACK;
    private final Font textFont = new Font("TimesNewRoman", 0, 24);
    private final String button1Text = "PLAY";
    private final Rectangle button1Box = new Rectangle(100, 100, 100, 30);
    
    
    private Button gameSelection;
    

    
   public MainMenu(){ 
       this.gameSelection = new Button(this.button1Box, this.button1Text, (this.buttonColor), this.textColor, textFont);
   }     

    
    public int update(double mouseX, double mouseY, boolean mouseButton) {
        int state = 0;
        
        if(mouseButton) {
            if(this.gameSelection.isClicked(mouseX, mouseY)){
                state = 1;
            }
        }
        
        
        
        return state;
    }
    
    
    public void draw(DConsole d) {
        this.gameSelection.draw(d);
    }
    
    
    
}
