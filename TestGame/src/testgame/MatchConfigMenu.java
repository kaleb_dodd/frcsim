/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame;

import DLibX.DConsole;
import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.awt.Rectangle;
import java.util.ArrayList;
import testgame.Elements.UI.Button;
import testgame.Elements.UI.IncrementalDisplay;

/**
 *
 * @author Kaleb
 */

public class MatchConfigMenu {
    private final Color buttonColor = Color.BLUE;
    private final Color textColor = Color.BLACK;
    private final Font textFont = new Font("TimesNewRoman", 0, 24);
    private final Font smallFont = new Font("TimesNewRoman", 0 , 16);
    private final String button1Text = "BACK";
    
   
    private final Rectangle button1Box = new Rectangle(300, 300, 100, 30);
    private final Rectangle startBox = new Rectangle(500,300,100,30);
   
    private ArrayList<Rectangle> increaseBoxes = new ArrayList<>(); 
    private ArrayList<Rectangle> decreaseBoxes = new ArrayList<>(); 
    private ArrayList<Rectangle> displayBoxes = new ArrayList<>();
   
    
    private Button back;
    private Button startButton;
    
    private ArrayList<Button> increaseButtons = new ArrayList<>();
    private ArrayList<Button> decreaseButtons = new ArrayList<>();
    private ArrayList<IncrementalDisplay> selections = new ArrayList<>();

    
    public MatchConfigMenu(){ 
        this.back = new Button(this.button1Box, this.button1Text, (this.buttonColor), this.textColor, this.textFont);
        this.startButton = new Button(this.startBox,"START", this.buttonColor, this.textColor, this.textFont);
       
        for(int i = 0; i < 2; i++){
            for(int j = 0; j < 4; j++){
                this.increaseBoxes.add(new Rectangle(200 + (i*300), 50 + j*50, 50, 30));
                this.increaseButtons.add(new Button(this.increaseBoxes.get(j + i*4), "+", buttonColor, textColor, textFont));
                this.decreaseBoxes.add(new Rectangle(20 + (i*300), 50 + j*50, 50, 30));
                this.decreaseButtons.add(new Button(this.decreaseBoxes.get(j + i*4), "-", buttonColor, textColor, textFont)); 
                this.displayBoxes.add(new Rectangle(80 + (i*300), 50 + j*50,110,30));
                this.selections.add(new IncrementalDisplay(this.displayBoxes.get(j + i*4)));
                if(i == 0){
                    this.selections.get(j + i*4).add("NONE");
                    this.selections.get(j + i*4).add("CPU");
                    this.selections.get(j + i*4).add("PLAYER"); 
                } else {
                    this.selections.get(j + i*4).add("1114M");
                    this.selections.get(j + i*4).add("1114Z");
                    this.selections.get(j + i*4).add("1114Y"); 
                }
                
            }    
        }
      
    }
    
    public ArrayList<String> getPlayerSelections(){
        ArrayList<String> selectedPlayers = new ArrayList<>();
        
        for(int i = 0; i < 4; i++){
            selectedPlayers.add(this.selections.get(i).getCurrentString());
        }
        
        return selectedPlayers;
    }
    
    public ArrayList<String> getRobotSelections(){
        ArrayList<String> selectedPlayers = new ArrayList<>();
        
        for(int i = 4; i < this.selections.size(); i++){
            selectedPlayers.add(this.selections.get(i).getCurrentString());
        }
        
        return selectedPlayers;
    }
    
    
   
  
    
    public int update(double mouseX, double mouseY, boolean mouseButton) {
        int state = 0;
        
        if(mouseButton) {
            if(this.back.isClicked(mouseX, mouseY)){
                state = 1;
            } else if(this.startButton.isClicked(mouseX, mouseY)) {
                state = 2;
            }
            
            for(int i = 0; i < this.increaseButtons.size(); i++){
               if(this.increaseButtons.get(i).isClicked(mouseX, mouseY)){
                   this.selections.get(i).update(1);
               }
            }
            
            for(int i = 0; i < this.decreaseButtons.size(); i++){
                if(this.decreaseButtons.get(i).isClicked(mouseX, mouseY)){
                    this.selections.get(i).update(-1);
                }
            }
            
        }
        
        
        
        return state;
    }
    
    
    public void draw(DConsole d) {
        this.back.draw(d);
        this.startButton.draw(d);
        
        for(Button b : this.increaseButtons){
            b.draw(d);
        }
        
        for(Button b : this.decreaseButtons){
            b.draw(d);
        }
        
         for(IncrementalDisplay dis : this.selections){
            dis.draw(d);
        }
         
         for(int i = 0; i < 4; i++){
             d.setFont(smallFont);
             if(i < 2){
                 d.setPaint(Color.RED);
             } else{
                 d.setPaint(Color.BLUE);
             }
             d.drawString("PLAYER " + (i+1), 100, 25+i*50);
         }
        
    }
    
    
    
}
