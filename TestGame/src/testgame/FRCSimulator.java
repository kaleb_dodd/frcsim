/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testgame;

import DLibX.DConsole;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import testgame.Elements.GenericMatch;
import testgame.Elements.NBN.NBNMatch;
import testgame.util.GamePads;
import testgame.util.Graphics;
import testgame.util.ResizableImage;
import testgame.util.Vect;

/**
 *
 * @author balke
 */
public class FRCSimulator {

    /**
     * @param args the command line arguments
     */
    private int width = 1000;
    private int height = 700;
    
    private DConsole d = new DConsole(this.width, this.height);
    private Graphics g = Graphics.getInstance();
    private GameState currentGameState = GameState.MAIN_MENU;
    private GameType currentGameType; 
    private RobotControl[] robotControls = new RobotControl[4];
    private PlayerControl[] playerControls = new PlayerControl[4];
    
    private Font defaultFont =  new Font("TimesNewRoman", 0, 24);
    private Color defaultTextColor = Color.BLACK;
    
    private BufferedImage img;
    private ResizableImage resImgTest; 
    
    private long startOfCycleTime;
    private double timeSinceLastCycle;
    private double fpsTarget = 60;
    private double currentFps;
    
    private MainMenu mainMenu;
    private GameSelectMenu gameSelectMenu;
    private MatchConfigMenu matchConfigMenu;
    private GenericMatch match; 
    private RobotHandler robotHandler; 
 
    
    public enum GameState{
        MAIN_MENU,
        GAME_SELECTION,
        MATCH_CONFIG,
        SETTINGS,
        PLAYING_MATCH,
        END_OF_MATCH,
    }
    
    public enum GameType{
        NOTHING_BUT_NET,
        ULTIMATE_ASCENT,
        STEAMWORKS, 
    }
    
    public enum RobotControl{
        NONE,
        CPU,
        PLAYER,
    }
    
    public enum PlayerControl{
        KEYBOARD,
        CONTROLLER,
    }
  

    
    
    public FRCSimulator() {
        this.g.setSize(this.width, this.height);
        this.g.setSizingRect(this.g.getScreenXPos(0.5), this.g.getScreenYPos(0.50),this.g.getScreenXPos(0.90),this.g.getScreenYPos(0.90));
        
        this.d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        this.d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        this.d.setOrigin(0);
        this.d.setResizable(false);
        this.mainMenu = new MainMenu();
        this.gameSelectMenu = new GameSelectMenu();
        this.matchConfigMenu = new MatchConfigMenu();
        
        
       
        
        
       

        while (true) {
            
            this.startOfCycleTime = System.currentTimeMillis();
            
            

            double mouseX = this.d.getMouseXPosition();
            double mouseY = this.d.getMouseYPosition();
            boolean isClicked = this.d.getMouseButton(1);
            
     
            if(this.currentGameState == GameState.MAIN_MENU){ 
                int menuresult = this.mainMenu.update(mouseX,mouseY ,isClicked);
            
                if(menuresult == 1){
                    this.currentGameState = GameState.GAME_SELECTION;
                }
            
                this.mainMenu.draw(d);
            
                this.d.drawString("menu " + menuresult, mouseX + 50, mouseY - 50);
            } else if (this.currentGameState == GameState.GAME_SELECTION) {
                int menuresult = this.gameSelectMenu.update(mouseX,mouseY ,isClicked);
            
                if(menuresult == 1){
                    this.currentGameState = GameState.MAIN_MENU;
                } else if(menuresult == 2){ //NOTHING BUT NET
                    this.currentGameState = GameState.MATCH_CONFIG;
                    this.currentGameType = GameType.NOTHING_BUT_NET;
                }
            
                this.gameSelectMenu.draw(d);
            } else if (this.currentGameState == GameState.MATCH_CONFIG) {
                int menuresult = this.matchConfigMenu.update(mouseX, mouseY, isClicked);
                
                if(menuresult == 1){
                    this.currentGameState = GameState.GAME_SELECTION;
                } else if(menuresult == 2){
                    this.currentGameState = GameState.PLAYING_MATCH;
                    if(this.currentGameType == GameType.NOTHING_BUT_NET){
                        this.match = new NBNMatch();
                        this.match.startMatch(System.currentTimeMillis());
                        this.robotHandler = new RobotHandler(currentGameType, 
                        this.matchConfigMenu.getPlayerSelections(), 
                        this.matchConfigMenu.getRobotSelections());
                        this.robotHandler.initMatch();
                        ArrayList<String> test = this.matchConfigMenu.getRobotSelections();
                        for(String t : test){
                            System.out.println(t);
                        }
                    }
                }
                
                this.matchConfigMenu.draw(d);
                
            } else if (this.currentGameState == GameState.PLAYING_MATCH){
                
                
                this.robotHandler.update(this.timeSinceLastCycle);
                this.match.update(System.currentTimeMillis());
                
                
                this.robotHandler.draw(d);
                this.match.draw(d);
                this.match.addScore(1, 1);
                
                this.d.drawString("TIME: " + this.match.getCurrentTime(), 10, 50);
                this.d.drawString("STATE: " + this.match.getCurrentMatchState(), 200, 50);
                
            }
            
            
            this.d.setFont(this.defaultFont);
            this.d.setPaint(this.defaultTextColor);
            this.d.drawString("STATE: " + this.currentGameState.toString(), 700, 10);
            
            
            /*//GAME LOGIC
            double x = 0;
            double y = 0;
            
            if(this.d.isKeyPressed('d')){
                x = 1.0;
            }else if(this.d.isKeyPressed('a')){
                x = -1.0;
            }
            if(this.d.isKeyPressed('s')){
                y = 1.0;
            }else if(this.d.isKeyPressed('w')){
                y = -1.0;
            }
            
            Vect inputVector = new Vect(x, y);
            if((x != 0 && y != 0)){  // moving diagonally 
                inputVector = inputVector.unit();
            }
            
            this.d.setPaint(Color.RED);
            
            // DRAW GAME
            this.g.debugDraw(d);
            Font f = new Font("TimesNewRoman", 0, 24);
            this.d.setFont(f);
            this.d.drawString("FPS: " + (int)Math.ceil(this.currentFps), 900, 50);
            this.d.drawString("TIME SINCE LAST CYCLE: " + (int)this.timeSinceLastCycle, 500, 60);
            this.resImgTest.draw(d);
            */
            
            this.d.redraw();
            
            // FPS CONTROL
            this.timeSinceLastCycle = System.currentTimeMillis() - this.startOfCycleTime;
            double pauseTime = (1000.0 / this.fpsTarget) - this.timeSinceLastCycle;

            if (pauseTime > 0) {
                this.currentFps = 1000.0 / (this.timeSinceLastCycle + pauseTime);
                this.d.pause((int)pauseTime);
                this.timeSinceLastCycle += pauseTime;

            } else {
                this.currentFps = 1000.0 / this.timeSinceLastCycle;
            }
            
            this.d.clear();

        }
    }

    public static void main(String[] args) {
        FRCSimulator g = new FRCSimulator();
    }
    


    
}
